function createCard(name, description, pictureUrl, location) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
    </div>
  `;
}


// window.addEventListener('DOMContentLoaded', async () => {

//       const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         console.error('Response failed. error code', error)
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();

//           const detailTag = document.querySelector('.card-text')
//           detailTag.innerHTML = details.conference.description;

//           const imageTag = document.querySelector('.card-img-top');
//           imageTag.setAttribute("src", details.conference.location.picture_url);


//         }
//             // Above is how to complete Data to DOM. It's the finishing portion. It gets the description data out of the object
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }


    window.addEventListener('DOMContentLoaded', async () => {

      const url = 'http://localhost:8000/api/conferences/';

      try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const location = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, location);
              const column = document.querySelector('.col');
              column.innerHTML += html;
            }
          }

        }
      } catch (e) {
        // Figure out what to do if an error is raised
      }
  });
